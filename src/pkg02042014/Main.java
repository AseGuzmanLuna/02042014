package pkg02042014;
import java.util.Scanner;
/**
 * Clase encargada de identificar intervalos
 * @author ASE Ü
 */
public class Main {
    static void funcion1 (float x){
        if(x<-15){
            System.out.println("Intervalo 1");
        }
        if(x>=-15){
            if(x<=-1){
            System.out.println ("Intervalo 2");    
            }
        }
        if(x>-1){
            if(x<10){
                System.out.println("Intervalo 3");
            }    
        } 
        if(x>=10){
            if(x<75){
                System.out.println("Intervalo 4");
            }    
        }
        if(x>=75){
            System.out.println("Intervalo 5");
        }
    }
    static void funcion2 (float x){
        if(x<-15)
        {
            System.out.println("Intervalo 1");
        }
        else
        {
            if(x<=-1)
            {
            System.out.println ("Intervalo 2");    
            }
            else
            {
                if(x<10)
                {
                System.out.println("Intervalo 3");
                }  
                else
                {
                    if(x<75)
                    {
                        System.out.println("Intervalo 4");
                    }
                    else
                    {
                        System.out.println("Intervalo 5");
                    }   
                }
            } 
        }   
    }


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        float x;
        System.out.println("Cual es el valor de x del que quieres saber su intervalo?");
        Scanner valores=new Scanner(System.in);
        x=valores.nextFloat();
        System.out.println("El intervalo donde "+x+" se encunetra es: ");
        funcion1(x);
        System.out.println("El intervalo donde "+x+" se encunetra es: ");
        funcion2(x);
    }
    
}
